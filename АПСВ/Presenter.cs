﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using АПСВ.Area;
using АПСВ.Algorithms;
using АПСВ.Grid;



namespace АПСВ
{
    class Presenter
    {

        private Form form;
        private ScaleView scale;
        private AreaView areaView;
        private List<AreaModel> saveAreas;
        private AreaModel tmp;
        private int numberArea;
        private EventArea eventArea;
        private int numberChangeArea;
        private int numberChangePoint;
        private String nameAreaChenge = null;
       

        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="form"></param>
        public Presenter(Form form)
        {
            this.form = form;
            scale = new ScaleView();
            areaView = new AreaView();
            saveAreas = new List<AreaModel>();
            numberArea = 0;
            eventArea = new EventArea();


        }

        /// <summary>
        /// Получения масштаба
        /// </summary>
        /// <returns></returns>
        internal ScaleView getScale()
        {
            return scale;
        }

        /// <summary>
        /// отрисовка области
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <param name="e"></param>
        internal void paintArea(PointF point1, PointF point2, PaintEventArgs e)
        {

            AreaModel areaModel = new AreaModel();
            areaModel.setPoint(point1, point2);

            areaView.paintArea(areaModel.getListPoint().ToArray(), e);

            areaModel.Polygon();


        }


        /// <summary>
        /// Отрисовка всех областей
        /// </summary>
        /// <param name="e"></param>
        internal void paintSaveArea(PaintEventArgs e)
        {
            foreach (var l in saveAreas)
            {
                areaView.paintSaveArea(l.getListPoint().ToArray(), e);
                if (l.getChecked())
                {
                    areaView.paintSelectedArea(l.getListPoint().ToArray(), e);
                }
            }
        }

        /// <summary>
        /// сохранение новой области
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        internal void saveArea(Point point1, Point point2)
        {
            AreaModel areaModel = new AreaModel();
            areaModel.setPoint(point1, point2);
            areaModel.setName(numberArea = numberArea + 1);
            saveAreas.Add(areaModel);
            tmp = areaModel;
        }


        /// <summary>
        /// Записывание в список
        /// </summary>
        /// <param name="dataGridView1"></param>
        internal void dataGridChange(DataGridView dataGridView1)
        {

            tmp.Polygon();
            dataGridView1.Rows.Add(tmp.getNameArea(), tmp.getArea());


            tmp = null;
        }


        internal void paintGrid(PaintEventArgs e)
        {
            throw new NotImplementedException();
        }




        /// <summary>
        /// Изменение грац области
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <param name="e"></param>
        internal void editAreaPoint(Point point1, Point point2, PaintEventArgs e)
        {
            foreach (var l in saveAreas)
            {
                if (l.getChecked())
                {
                    if (l.getNameArea().Equals(nameAreaChenge))
                    {
                        eventArea.changePointArea(l, numberChangePoint, point2);

                    }
                }
            }
        }

        /// <summary>
        /// Изменение значения вершины
        /// </summary>
        /// <param name="point2"></param>
        /// <param name="e"></param>
        internal void changeArea(Point point2, PaintEventArgs e)
        {
            foreach (var l in saveAreas)
            {
                if (l.getChecked())
                {
                    if (l.getNameArea().Equals(nameAreaChenge))
                    {
                        eventArea.changePointArea(l, numberChangePoint, point2);

                    }
                }
            }
        }

        /// <summary>
        /// выделение области
        /// </summary>
        /// <param name="e"></param>
        internal void difineAreaPoint(MouseEventArgs e)
        {
            foreach (var l in saveAreas)
            {

                List<PointF> points = l.getListPoint();
                for (int i = 0; i <= points.Count - 1; i++)
                {
                    if (e.X >= points[i].X - 3 &&
                        e.X <= points[i].X + 3 &&
                        e.Y >= points[i].Y - 3 &&
                        e.Y <= points[i].Y + 3)
                    {
                        nameAreaChenge = l.getNameArea();
                        numberChangePoint = i;

                    }
                }
            }
        }

        /// <summary>
        /// Сброс указателей изменений
        /// </summary>
        internal void resetChangeArea()
        {
            nameAreaChenge = null;

        }

        /// <summary>
        /// Построение сетки размещения
        /// </summary>
        /// <param name="pictureBox1"></param>
        internal void buildGrid(PictureBox pictureBox1,PaintEventArgs e)
        {
            foreach (AreaModel l in saveAreas) {
                l.buildGrid(
                    pictureBox1.Height,
                    pictureBox1.Width);
                
                new GridView().paintGrid(l.getGrid(), pictureBox1, e);
                
            }


        }

        /// <summary>
        /// Изменение Изменение панели компонентов области
        /// </summary>
        /// <param name="dataGridView1"></param>
        internal void chargeDataGridView(DataGridView data)
        {
            data.Rows.Clear();
            foreach (AreaModel l in saveAreas)
            {

                l.Polygon();

                data.Rows.Add(l.getNameArea(), l.getArea());
            }
        }

        /// <summary>
        /// Проверка на расположение нескольких областей
        /// </summary>
        /// <param name="e"></param>
        internal void setSelectedAreaShift(MouseEventArgs e, DataGridView data)
        {
            eventArea.AreaClickShift(saveAreas, e);
            data.ClearSelection();
            foreach (AreaModel l in saveAreas)
            {
                for (int j = 0; j < data.RowCount - 1; j++)
                {

                    if (data.Rows[j].Cells[0].Value.ToString() == l.getNameArea())
                    {
                        if (l.getChecked())
                        {
                            data.Rows[j].Selected = true;
                        }

                    }
                }
            }
        }



        /// <summary>
        /// Метод задает проверку на расположение области
        /// </summary>
        /// <param name="e">координаты </param>
        internal void setSelectedArea(MouseEventArgs e, DataGridView data)
        {

            eventArea.AreaClick(saveAreas, e);
            data.ClearSelection();

            foreach (AreaModel l in saveAreas)
            {

                for (int j = 0; j < data.RowCount - 1; j++)
                {

                    if (data.Rows[j].Cells[0].Value.ToString() == l.getNameArea())
                    {
                        if (l.getChecked())
                        {
                            data.Rows[j].Selected = true;
                        }

                    }
                }
            }

        }

        /// <summary>
        /// Слияние областей двух областей
        /// </summary>
        internal void margeAreas()
        {

            bool a = false;
            bool b = false;
            AreaModel areaModel1 = null;
            AreaModel areaModel2 = null;
            

            if (saveAreas.Count > 1)
            {
                for (int i = 0; i < saveAreas.Count(); i++)
                {
                    if (saveAreas[i].getChecked())
                    {
                        if (b == false && a == true)
                        {
                            areaModel2 = saveAreas[i];
                            saveAreas.Remove(saveAreas[i]);
                        }
                        if (a == false)
                        {
                            areaModel1 = saveAreas[i];

                            a = true;
                        }
                    }
                }

                try { eventArea.margeAreas(areaModel1, areaModel2); }
                catch
                {
                    MessageBox.Show("Выберите две области для слияния", "Сообщение");
                }

            }
        }

        /// <summary>
        /// Удаление выбранных областей
        /// </summary>
        internal void deletedArea()
        {
            List<AreaModel> newSaveArea = new List<AreaModel>();
            foreach (var l in saveAreas)
            {

                if (!l.getChecked())
                {
                    newSaveArea.Add(l);
                }

            }
            saveAreas = newSaveArea;
        }
    }
}