﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace АПСВ.Grid
{
    public class Node
    {
        private PointF point;

        public void setNode(PointF point)
        {
            this.point = point;
        }

        public PointF getPoint()
        {
            return point;
        }

        

        public bool IsInPolygon(PointF[] poly, PointF p)
        {
            PointF p1, p2;
            bool inside = false;
            if (poly.Length < 3) { return inside; }
            var oldPoint = new PointF(poly[poly.Length - 1].X, poly[poly.Length - 1].Y);
            for (int i = 0; i < poly.Length; i++)
            {
                var newPoint = new PointF(poly[i].X, poly[i].Y);
                if (newPoint.X > oldPoint.X) { p1 = oldPoint; p2 = newPoint; }
                else { p1 = newPoint; p2 = oldPoint; }

                if ((newPoint.X < p.X) == (p.X <= oldPoint.X)
                    && (p.Y - (long)p1.Y) * (p2.X - p1.X) < (p2.Y - (long)p1.Y) * (p.X - p1.X))
                { inside = !inside; }
                oldPoint = newPoint;
            }
            return inside;
        }

        
    }
}
