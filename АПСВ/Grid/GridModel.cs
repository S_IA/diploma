﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using АПСВ.Grid;

namespace АПСВ.Grid
{
    class GridModel
    {
        private String areaName; // Название области
        private Node[] nodes;     // Массив узлов

        internal Node[] getNodes()
        {
            return nodes;
        }

        public GridModel(String name)
        {
            areaName = name;
        }

        public void setName(String name)
        {
            areaName = name;
        }

        public String getName()
        {
            return areaName;
        }

        /// <summary>
        /// Построение сетки
        /// </summary>
        /// <param name="height"> высота</param>
        /// <param name="width"> ширина</param>
        /// <param name="listPoint"> координаты вершин области</param>
        internal void buildGrid(int height, int width, List<PointF> listPoint, int density )
        {
            PointF[] points = new PointF[listPoint.Count];

            for (int i = 0; i <listPoint.Count; i++) {
                points[i] = listPoint[i];
            }

            List<Node> listNode = new List<Node>();
            for (int i = 0; i < width; i=i+density)
            {
                for (int j = 0; j < height; j=j+25) {
                    PointF tmpPoint = new PointF(i,j);
                    Node tmpNode = new Node();
                    tmpNode.setNode(tmpPoint);
                    if (tmpNode.IsInPolygon(points, tmpPoint)) {
                        listNode.Add(tmpNode);
                    }

                }
            }

            nodes = new Node[listNode.Count];
            for (int i = 0; i < listNode.Count; i++)
            {
                nodes[i] = listNode[i];
            }
        }
    }
}
