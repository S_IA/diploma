﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace АПСВ.Grid
{
    class GridView
    {
       

        internal void paintGrid(GridModel gridModel, PictureBox pictureBox1, PaintEventArgs e)
        {
            int countNodes = gridModel.getNodes().Length;

            for (int i = 0; i < countNodes; i++)
            {

                Pen blackPen = new Pen(Color.Black, 3);
               
                   e.Graphics.DrawLine(
                    blackPen,
                    gridModel.getNodes()[i].getPoint().X - 5,
                    gridModel.getNodes()[i].getPoint().Y,
                    gridModel.getNodes()[i].getPoint().X + 5,
                    gridModel.getNodes()[i].getPoint().Y);

                e.Graphics.DrawLine(
                    blackPen,
                    gridModel.getNodes()[i].getPoint().X ,
                    gridModel.getNodes()[i].getPoint().Y -5,
                    gridModel.getNodes()[i].getPoint().X ,
                    gridModel.getNodes()[i].getPoint().Y + 5);


            }
        }
    }
}
