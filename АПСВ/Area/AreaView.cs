﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace АПСВ
{
    public class AreaView
    {
        List<Point> pointList;

        public AreaView()
        {
            pointList = new List<Point>();
        }

        public void paintArea(PointF[] points, PaintEventArgs paint)
        {
            Pen pen = new Pen(Color.Black, 2);

            // Draw polygon to screen.
            paint.Graphics.DrawPolygon(pen, points);
            pen.Dispose();

        }

        public void paintSaveArea( PointF[] points, PaintEventArgs paint)
        {
            Pen bluePen = new Pen(Color.Blue, 3);
            paint.Graphics.DrawPolygon(bluePen, points);
            bluePen.Dispose();
        }



        public List<Point> getListPoint()
        {
            return pointList;
        }

        internal void paintSelectedArea(PointF[] points, PaintEventArgs e)
        {
            Pen bluePen = new Pen(Color.Blue, 4);
            e.Graphics.DrawPolygon(bluePen, points);
            bluePen.Dispose();
            for (int i = 0; i < points.Length; i++)
            {
                SolidBrush redPen =new SolidBrush(Color.GreenYellow);
                e.Graphics.FillEllipse(redPen, points[i].X-5, points[i].Y-5, 10, 10);
            }
        }
    }




}
