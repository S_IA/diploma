﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using АПСВ.Area;
using АПСВ.Algorithms;
using АПСВ.Grid;


namespace АПСВ
{
    public class AreaModel
    {

        private List<PointF> listPoint;         // Вершины 
        private List<PointF> initialListPoint;  // Инициализированные вершины
        private Triangle[] triangles;           // Треугольники, на который разбит многоугольник
        private bool[] taken;                   // Была рассмотрена i-вершина
        private String name;                    // Название области
        private Boolean checkedArea = false;    // Состояние области
        private double S;                       // Площадь
        private GridModel grid;                 //Сетка размещений


        /// <summary>
        /// </summary>
        public AreaModel()
        {
            listPoint = new List<PointF>();
        }

        /// <summary>
        /// Получить сетку
        /// </summary>
        /// <returns></returns>
        internal GridModel getGrid() {
            return grid;
        }

        ///////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Подготовка к триангуляции
        /// </summary>
        public void Polygon()
        {
            PointF[] points = new PointF[listPoint.Count - 1];

            for (int i = 0; i < listPoint.Count - 1; i++)
            {
                points[i] = listPoint[i];
            }
            triangles = new Triangle[points.Length - 2];
            taken = new bool[points.Length];
            new Triangulate().
                triangulate(points, triangles, taken); //триангуляция
        }

        /// <summary>
        /// Была просмотрена
        /// </summary>
        /// <returns></returns>
        internal bool[] getTaken()
        {
            return taken;
        }

        /// <summary>
        /// Получить площадь
        /// </summary>
        /// <returns></returns>
        public double getArea()
        {
            if (triangles == null)
            {
                S = 0; //площадь нулевая, если триангуляция не удалась
                return S;
            }


            S = 0;
            for (int i = 0; i < triangles.Length; i++) //суммируем площади для каждого треугольника
                S += triangles[i].getArea();

            return Math.Round(S, 1);
        }

        /// <summary>
        /// Получить массив триугольников
        /// </summary>
        /// <returns></returns>
        Triangle[] getTriangles()
        {
            return triangles;
        }





        /////////////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Метод задачи вершин созданного прямоугольника
        /// </summary>
        /// <param name="point1">Первая вершины</param>
        /// <param name="point2">Вторая вершина(противополжная первой)</param>
        public void setPoint(PointF point1, PointF point2)
        {

            listPoint.Clear();

            Pen pen = new Pen(Color.Black, 2);
            PointF[] massPoint = new PointF[4];
            Pen blackPen = new Pen(Color.Black, 3);

            PointF[] curvePoints =
                     {
                 point1,
                 new PointF(point1.X, point2.Y),
                 point2,
                 new PointF(point2.X, point1.Y)
             };

            listPoint.Add(point1);                          //нижний левый угол
            listPoint.Add(new PointF(point1.X, point2.Y));   //верхний левый угол
            listPoint.Add(point2);                          //верхний правый угол
            listPoint.Add(new PointF(point2.X, point1.Y));   //нижний правый угол

            initialListPoint = listPoint;
        }

        /// <summary>
        /// Получить список вершин
        /// </summary>
        /// <returns></returns>
        internal List<PointF> getListPoint()
        {
            return listPoint;
        }

        /// <summary>
        /// Получить статус
        /// true - область выделена
        /// false - область 
        /// </summary>
        /// <returns></returns>
        internal bool getChecked()
        {
            return checkedArea;
        }

        /// <summary>
        /// Получить название области
        /// </summary>
        /// <returns></returns>
        internal String getNameArea()
        {
            return name;
        }

        /// <summary>
        /// задать имя области
        /// </summary>
        /// <param name="n"></param>
        internal void setName(int n)
        {
            name = "A" + n;
        }

        /// <summary>
        /// Изменить вершину области
        /// </summary>
        /// <param name="point"></param>
        internal void changeArea(Point point)
        {
            listPoint[0] = point;
        }

        /// <summary>
        /// Изменить состояние на true
        /// </summary>
        internal void Checked()
        {

            checkedArea = true;
        }

        /// <summary>
        /// Изменить состояние на folse
        /// </summary>
        internal void notChecked()
        {
            checkedArea = false;
        }

        /// <summary>
        /// Построить сетку размещения
        /// </summary>
        /// <param name="height">высота области отрисовки</param>
        /// <param name="width">ширина области отрисовки</param>
        internal void buildGrid(int height, int width)
        {
            grid = new GridModel(name);
            grid.buildGrid(height, width, listPoint, 25);

        }

        /// <summary>
        /// Получить инициализированные вершины
        /// </summary>
        /// <returns></returns>
        internal List<PointF> getInitialListPoint()
        {
            return initialListPoint;
        }

        /// <summary>
        /// Изменить вершину области
        /// </summary>
        /// <param name="i"> номер вершины</param>
        /// <param name="point2"> новое значение вершины </param>
        internal void changeArea(int i, Point point2)
        {
            listPoint[i] = point2;
        }


    }
}
