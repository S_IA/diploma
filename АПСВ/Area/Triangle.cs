﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace АПСВ.Area
{
    class Triangle
    {
        private PointF a, b, c;

        public Triangle(PointF a, PointF b, PointF c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public PointF getA()
        {
            return a;
        }

        public PointF getB()
        {
            return b;
        }

        public PointF getC()
        {
            return c;
        }

        /// <summary>
        /// подсчет площади треугольника
        /// </summary>
        /// <returns></returns>
        public double getArea() 
        {
            //длины трех сторон
            double a = storona(this.a, this.b);
            double b = storona(this.b, this.c);
            double c = storona(this.c, this.a);

            double p = (a + b + c) / 2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c)); //площадь по трем сторонам
        }

        /// <summary>
        /// подсчет длины отрезка ab
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private double storona(PointF a, PointF b) 
        {
            float x = a.X - b.X;
            float y = a.Y - b.Y;
            return Math.Sqrt(x * x + y * y);
        }
    }
}
