﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.CSharp;
using АПСВ.Algorithms;

namespace АПСВ.Area
{
    class EventArea
    {
        /// <summary>
        /// Событие клика области при клике
        /// </summary>
        /// <param name="list">Список областей</param>
        /// <param name="e">координаты клика</param>
        public void AreaClick(List<AreaModel> list, MouseEventArgs e)
        {
            foreach (var l in list)
            {
                l.notChecked();
                List<PointF> points = l.getListPoint();
                for (int i = 0; i < points.Count; i++)
                {
                    // последняя точка
                    if (i == points.Count - 1)
                    {
                        //находятся на одной линии оси Х
                        if (points[i].X == points[0].X)
                        {
                            // точка i ниже точки 0
                            if (points[i].Y > points[0].Y)
                            {
                                if (e.X > points[i].X - 5 &&
                                e.X < points[i].X + 2 &&
                                e.Y > points[0].Y &&
                                e.Y < points[i].Y)
                                {
                                    l.Checked();
                                }
                            }
                            //точка i выше точки 0
                            if (points[i].Y < points[0].Y)
                            {
                                if (e.X > points[i].X - 5 &&
                                e.X < points[i].X + 2 &&
                                e.Y < points[0].Y &&
                                e.Y > points[i].Y)
                                {
                                    l.Checked();
                                }
                            }
                        }

                        //находиться на одной оси Y
                        if (points[i].Y == points[0].Y)
                        {

                            // точка i дальше точки 0
                            if (points[i].X > points[0].X)
                            {

                                if (e.Y > points[0].Y - 2 &&
                                     e.Y < points[i].Y + 2 &&
                                     e.X < points[i].X &&
                                     e.X > points[0].X)
                                {
                                    l.Checked();
                                }

                            }

                            // точка i ближе точки 0
                            if (points[i].X < points[0].X)
                            {

                                if (e.Y > points[i].Y - 2 &&
                                     e.Y < points[i].Y + 2 &&
                                     e.X > points[i].X &&
                                     e.X < points[0].X)
                                {
                                    l.Checked();
                                }
                            }
                        }
                    }
                    else
                    {
                        //находятся на одной линии оси Х
                        if (points[i].X == points[i + 1].X)
                        {
                            // точка i ниже точки i+1
                            if (points[i].Y > points[i + 1].Y)
                            {
                                if (e.X > points[i].X - 5 &&
                                e.X < points[i].X + 2 &&
                                e.Y > points[i + 1].Y &&
                                e.Y < points[i].Y)
                                {
                                    l.Checked();
                                }
                            }
                            //точка i выше точки i+1
                            if (points[i].Y < points[i + 1].Y)
                            {
                                if (e.X > points[i].X - 5 &&
                                e.X < points[i].X + 2 &&
                                e.Y < points[i + 1].Y &&
                                e.Y > points[i].Y)
                                {
                                    l.Checked();
                                }
                            }

                        }

                        //находиться на одной оси Y
                        if (points[i].Y == points[i + 1].Y)
                        {

                            // точка i дальше точки i+1
                            if (points[i].X > points[i + 1].X)
                            {

                                if (e.Y > points[i + 1].Y - 2 &&
                                     e.Y < points[i].Y + 2 &&
                                     e.X < points[i].X &&
                                     e.X > points[i + 1].X)
                                {
                                    l.Checked();
                                }

                            }

                            // точка i ближе точки i+1
                            if (points[i].X < points[i + 1].X)
                            {

                                if (e.Y > points[i].Y - 2 &&
                                     e.Y < points[i].Y + 2 &&
                                     e.X > points[i].X &&
                                     e.X < points[i + 1].X)
                                {
                                    l.Checked();
                                }
                            }
                        }
                    }

                }



            }
        }

        /// <summary>
        /// Слияние
        /// </summary>
        /// <param name="areaModel1"></param>
        /// <param name="areaModel2"></param>
        internal void margeAreas(AreaModel areaModel1, AreaModel areaModel2)
        {

            List<PointF> points1 = areaModel1.getListPoint();
            List<PointF> points2 = areaModel2.getListPoint();
            List<PointF> newPoints = new List<PointF>();

            PointF maxPoint1 = new SortPoint().getMaxPoint(points1);
            PointF minPoint1 = new SortPoint().getMinPoint(points1);
            PointF LeftPoint1 = new SortPoint().getLeftPoint(points1);
            PointF RightPoint1 = new SortPoint().getRightPoint(points1);

            PointF maxPoint2 = new SortPoint().getMaxPoint(points2);
            PointF minPoint2 = new SortPoint().getMinPoint(points2);
            PointF LeftPoint2 = new SortPoint().getLeftPoint(points2);
            PointF RightPoint2 = new SortPoint().getRightPoint(points2);






            bool isReverse = false;

            if (maxPoint2.Y < minPoint1.Y)
            {

                // Вторая область: нуль вершина выше первой
                if (points2[0].Y < points2[1].Y)
                {

                    //Прокрутка вершин в списке
                    while ((points2[0].Y == minPoint2.Y))
                    {
                        PointF tmp = points2[points2.Count - 1];
                        for (int i = points2.Count - 1; i != 0; --i)
                        {
                            points2[i] = points2[i - 1];
                        }
                        points2[0] = tmp;
                    }
                    // points2.Reverse();

                }


            }
            if (maxPoint1.Y < minPoint2.Y)
            {

                // Вторая область: нуль вершина ниже первой
                if (points2[0].Y > points2[1].Y)
                {
                   
                    //Прокрутка вершин в списке
                    while ((points2[0].Y == maxPoint2.Y))
                    {
                        PointF tmp = points2[points2.Count - 1];
                        for (int i = points2.Count - 1; i != 0; --i)
                        {
                            points2[i] = points2[i - 1];
                        }
                        points2[0] = tmp;
                    }
                    // points2.Reverse();

                }

                newPoints = points1;

                int indexPoints = 0;

                for (int i = 0; i < points1.Count; i++)
                {
                    if (maxPoint1 == points1[i])
                    {
                        indexPoints = i;
                    }
                }

                for (int i = 0; i < points2.Count; i++)
                {
                    newPoints.Insert(indexPoints + 1 + i, points2[i]);
                }

            }
            else
            {
                newPoints = points1;
                newPoints.AddRange(points2);
            }

            PointF[] array = new PointF[newPoints.Count - 1];
            for (int i = 0; i < newPoints.Count - 1; i++)
            {
                array[i] = newPoints[i];
            }

        }

        /// <summary>
        /// Событие клика области при клике с зажатым Shift
        /// </summary>
        /// <param name="list">Список областей</param>
        /// <param name="e">координаты клика</param>
        public void AreaClickShift(List<AreaModel> list, MouseEventArgs e)
        {
            foreach (var l in list)
            {

                List<PointF> points = l.getListPoint();
                for (int i = 0; i < points.Count; i++)
                {
                    // последняя точка
                    if (i == points.Count - 1)
                    {
                        //находятся на одной линии оси Х
                        if (points[i].X == points[0].X)
                        {
                            // точка i ниже точки 0
                            if (points[i].Y > points[0].Y)
                            {
                                if (e.X > points[i].X - 5 &&
                                e.X < points[i].X + 2 &&
                                e.Y > points[0].Y &&
                                e.Y < points[i].Y)
                                {
                                    l.Checked();
                                }
                            }
                            //точка i выше точки 0
                            if (points[i].Y < points[0].Y)
                            {
                                if (e.X > points[i].X - 5 &&
                                e.X < points[i].X + 2 &&
                                e.Y < points[0].Y &&
                                e.Y > points[i].Y)
                                {
                                    l.Checked();
                                }
                            }
                        }

                        //находиться на одной оси Y
                        if (points[i].Y == points[0].Y)
                        {

                            // точка i дальше точки 0
                            if (points[i].X > points[0].X)
                            {

                                if (e.Y > points[0].Y - 2 &&
                                     e.Y < points[i].Y + 2 &&
                                     e.X < points[i].X &&
                                     e.X > points[0].X)
                                {
                                    l.Checked();
                                }

                            }

                            // точка i ближе точки 0
                            if (points[i].X < points[0].X)
                            {

                                if (e.Y > points[i].Y - 2 &&
                                     e.Y < points[i].Y + 2 &&
                                     e.X > points[i].X &&
                                     e.X < points[0].X)
                                {
                                    l.Checked();
                                }
                            }
                        }
                    }
                    else
                    {
                        //находятся на одной линии оси Х
                        if (points[i].X == points[i + 1].X)
                        {
                            // точка i ниже точки i+1
                            if (points[i].Y > points[i + 1].Y)
                            {
                                if (e.X > points[i].X - 5 &&
                                e.X < points[i].X + 2 &&
                                e.Y > points[i + 1].Y &&
                                e.Y < points[i].Y)
                                {
                                    l.Checked();
                                }
                            }
                            //точка i выше точки i+1
                            if (points[i].Y < points[i + 1].Y)
                            {
                                if (e.X > points[i].X - 5 &&
                                e.X < points[i].X + 2 &&
                                e.Y < points[i + 1].Y &&
                                e.Y > points[i].Y)
                                {
                                    l.Checked();
                                }
                            }

                        }

                        //находиться на одной оси Y
                        if (points[i].Y == points[i + 1].Y)
                        {

                            // точка i дальше точки i+1
                            if (points[i].X > points[i + 1].X)
                            {

                                if (e.Y > points[i + 1].Y - 2 &&
                                     e.Y < points[i].Y + 2 &&
                                     e.X < points[i].X &&
                                     e.X > points[i + 1].X)
                                {
                                    l.Checked();
                                }

                            }

                            // точка i ближе точки i+1
                            if (points[i].X < points[i + 1].X)
                            {

                                if (e.Y > points[i].Y - 2 &&
                                     e.Y < points[i].Y + 2 &&
                                     e.X > points[i].X &&
                                     e.X < points[i + 1].X)
                                {
                                    l.Checked();
                                }
                            }
                        }
                    }

                }



            }
        }

        /// <summary>
        /// Изменение кординат верши области
        /// </summary>
        /// <param name="l"></param>
        /// <param name="numberChangePoint"></param>
        /// <param name="point2"></param>
        internal void changePointArea(AreaModel l, int numberChangePoint, Point point2)
        {
            int coutPoint = l.getListPoint().Count;

            if (numberChangePoint == 0 || numberChangePoint == l.getListPoint().Count - 1)
            {
                if (numberChangePoint == 0)
                {
                    if (l.getListPoint()[numberChangePoint].X == l.getListPoint()[numberChangePoint + 1].X)
                    {
                        PointF point = new PointF();
                        point.Y = l.getListPoint()[index: numberChangePoint + 1].Y;
                        point.X = point2.X;
                        l.getListPoint()[index: numberChangePoint + 1] = point;
                    }

                    if (l.getListPoint()[numberChangePoint].Y == l.getListPoint()[coutPoint - 1].Y)
                    {
                        PointF point = new PointF();
                        point.X = l.getListPoint()[index: coutPoint - 1].X;
                        point.Y = point2.Y;
                        l.getListPoint()[index: coutPoint - 1] = point;
                    }

                }
                if (numberChangePoint == l.getListPoint().Count - 1)
                {
                    if (l.getListPoint()[numberChangePoint].X == l.getListPoint()[numberChangePoint - 1].X)
                    {
                        PointF point = new PointF();
                        point.Y = l.getListPoint()[numberChangePoint - 1].Y;
                        point.X = point2.X;
                        l.getListPoint()[numberChangePoint - 1] = point;
                    }

                    if (l.getListPoint()[numberChangePoint].Y == l.getListPoint()[0].Y)
                    {
                        PointF point = new PointF();
                        point.X = l.getListPoint()[0].X;
                        point.Y = point2.Y;
                        l.getListPoint()[0] = point;
                    }

                }
            }
            else
            {
                if (l.getListPoint()[numberChangePoint].X == l.getListPoint()[numberChangePoint + 1].X)
                {
                    PointF point = new PointF();
                    point.Y = l.getListPoint()[index: numberChangePoint + 1].Y;
                    point.X = point2.X;
                    l.getListPoint()[index: numberChangePoint + 1] = point;
                }

                if (l.getListPoint()[numberChangePoint].Y == l.getListPoint()[numberChangePoint - 1].Y)
                {
                    PointF point = new PointF();
                    point.X = l.getListPoint()[numberChangePoint - 1].X;
                    point.Y = point2.Y;
                    l.getListPoint()[numberChangePoint - 1] = point;
                }

                if (l.getListPoint()[numberChangePoint].Y == l.getListPoint()[numberChangePoint + 1].Y)
                {
                    PointF point = new PointF();
                    point.X = l.getListPoint()[index: numberChangePoint + 1].X;
                    point.Y = point2.Y;
                    l.getListPoint()[index: numberChangePoint + 1] = point;
                }

                if (l.getListPoint()[numberChangePoint].X == l.getListPoint()[numberChangePoint - 1].X)
                {
                    PointF point = new PointF();
                    point.Y = l.getListPoint()[numberChangePoint - 1].Y;
                    point.X = point2.X;
                    l.getListPoint()[numberChangePoint - 1] = point;
                }

            }

            l.getListPoint()[numberChangePoint] = point2;

        }
    }
}