﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace АПСВ
{
    public partial class Form1 : Form
    {
        Bitmap image;
        Graphics graphics;
        public Point point1;
        public Point point2;
        bool isClicked = false;
        bool isClickedButton3 = false;
        Presenter presenter;

        //--------test----------//

        List<Point> points;
        //----------------------//

        public Form1()
        {
            InitializeComponent();
            presenter = new Presenter(this);
            points = new List<Point>();

        }

        private void выйтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void загрузитьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();

            openDialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат загружаемого файла
            if (openDialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(openDialog.FileName);
                    //вместо pictureBox1 укажите pictureBox, в который нужно загрузить изображение 

                    pictureBox1.Image = image;
                    pictureBox1.BackgroundImageLayout = ImageLayout.Center;
                    pictureBox1.Invalidate();
                    graphics = Graphics.FromImage(image);
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void поворот90ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Image.RotateFlip(RotateFlipType.Rotate90FlipXY);
            pictureBox1.Invalidate();
        }

        //---------------отрисовка-----------------//
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            presenter.paintSaveArea(e);

            //Строить маштаб
            if (checkBox1.Checked)
            {
                presenter.getScale().SizeScaler(point1, point2, e, textBox1, textBox2);
            }

            //Строить область
            if (checkBox2.Checked)
            {
                presenter.paintArea(point1, point2, e);
            }

            //Изменяет координаты вершин оьласти
            if (!checkBox2.Checked) {
               presenter.changeArea(point2, e);
                presenter.chargeDataGridView(dataGridView1);
            }

            if (isClickedButton3) {

                presenter.buildGrid(pictureBox1, e);
                isClickedButton3 = false;
            }
          

           
        }

        //-------------нажатие мыши-----------------//
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            isClicked = true;
            point1 = e.Location;

            presenter.difineAreaPoint(e);
          
           
        }

        //------------зажатие клавиши мышки---------//
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isClicked)
            {
                point2 = e.Location;
                pictureBox1.Invalidate();
                
            }

            
        }
        //-----------------отпустил мышку-----------//
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            isClicked = false;

            if (checkBox2.Checked)
            {
                presenter.saveArea(point1, point2);
                presenter.dataGridChange(dataGridView1);
            }

           
            if (!checkBox2.Checked) {
                presenter.resetChangeArea();
            }

        }


        
        //----------------клик мышкой----------------//
        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {

            if (!checkBox2.Checked
                && !(Control.ModifierKeys.HasFlag(Keys.Shift)))
            {
                presenter.setSelectedArea(e, dataGridView1);
                int x = e.Y;
                textBox1.Text = Convert.ToString(x);
                pictureBox1.Invalidate();
            }

            if (!checkBox2.Checked
                && (Control.ModifierKeys.HasFlag(Keys.Shift))) {
                presenter.setSelectedAreaShift(e, dataGridView1);
                int x = e.Y;
                textBox1.Text = Convert.ToString(x);
                pictureBox1.Invalidate();
            }

        }

        //----------------выбор ячейки спииска области
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        /// <summary>
        /// Удаление периметра области 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            presenter.deletedArea();
            presenter.chargeDataGridView(dataGridView1);
            pictureBox1.Invalidate();
        }

        /// <summary>
        /// Слияние
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            presenter.margeAreas();
            presenter.chargeDataGridView(dataGridView1);
            pictureBox1.Invalidate();
        }


        /// <summary>
        /// Построить сетку расположений для выбраных областей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            isClickedButton3 = true;
            pictureBox1.Invalidate();
        }
    }
}
