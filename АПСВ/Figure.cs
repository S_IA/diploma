﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace АПСВ
{
    abstract public class Figure
    {
        abstract public double Area();    // get area
        abstract public double Perimeter();    //get perimetr
        abstract public void Show();    //show information
    }


}
