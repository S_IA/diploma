﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace АПСВ
{
    public class ScaleView 
    {

        int distance;
        int s2;
        private static Pen pen = new Pen(Color.Black);


        public ScaleView()
        {
        }

        public void SizeScaler(Point p1, Point p2, PaintEventArgs paint,
            TextBox textBox1,
            TextBox textBox2)
        {

            if (p1.Y > p2.Y)
            {
                distance = p1.Y - p2.Y;
                textBox1.Text = Convert.ToString(distance);
            }
            else
            {
                distance = p2.Y - p1.Y;
                textBox1.Text = Convert.ToString(distance);
            }

            Pen pen = new Pen(Color.Black, 2);
            paint.Graphics.DrawLine(pen, p1.X, p1.Y, p1.X, p2.Y);
            paint.Graphics.DrawLine(pen, p1.X - 5, p1.Y, p1.X + 5, p1.Y);
            paint.Graphics.DrawLine(pen, p1.X + 5, p2.Y, p1.X - 5, p2.Y);
            pen.Dispose();

        }
    }
}
