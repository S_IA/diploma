﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace АПСВ.Algorithms
{
    class SortPoint
    {
        internal PointF getMaxPoint(List<PointF> points)
        {
            PointF point = points[0];
            foreach (PointF p in points)
            {
                if (p.Y > point.Y)
                {
                    point = p;
                }
            }
            return point;
        }

        internal PointF getMinPoint(List<PointF> points)
        {
            PointF point = points[0];
            foreach (PointF p in points)
            {
                if (p.Y < point.Y)
                {
                    point = p;
                }
            }
            return point;
        }

        internal PointF getLeftPoint(List<PointF> points)
        {
            PointF point = points[0];
            foreach (PointF p in points)
            {
                if (p.X < point.X)
                {
                    point = p;
                }
            }
            return point;
        }

        internal PointF getRightPoint(List<PointF> points)
        {
            PointF point = points[0];
            foreach (PointF p in points)
            {
                if (p.X > point.X)
                {
                    point = p;
                }
            }
            return point;
        }
    }


}
