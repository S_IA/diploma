﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using АПСВ.Area;

namespace АПСВ.Algorithms
{
    class Triangulate
    {

        

        /// <summary>
        /// Триангуляция
        /// </summary>
        /// <param name="points"> вершины</param>
        public void triangulate(PointF[] points, Triangle[] triangles, bool[] taken)
        {
            int trainPos = 0; //
            int leftPoints = points.Length; //сколько осталось рассмотреть вершин

            //текущие вершины рассматриваемого треугольника
            int ai = findNextNotTaken(points, 0, taken);
            int bi = findNextNotTaken(points, ai + 1, taken);
            int ci = findNextNotTaken(points, bi + 1, taken);

            int count = 0; //количество шагов

            while (leftPoints > 3) //пока не остался один треугольник
            {
                if (isLeft(points[ai], points[bi], points[ci]) && canBuildTriangle(points, ai, bi, ci)) //если можно построить треугольник
                {
                    triangles[trainPos++] = new Triangle(points[ai], points[bi], points[ci]); //новый треугольник
                    taken[bi] = true; //исключаем вершину b
                    leftPoints--;
                    bi = ci;
                    ci = findNextNotTaken(points, ci + 1, taken); //берем следующую вершину
                }
                else
                { //берем следующие три вершины
                    ai = findNextNotTaken(points, ai + 1, taken);
                    bi = findNextNotTaken(points, ai + 1, taken);
                    ci = findNextNotTaken(points, bi + 1, taken);
                }

                if (count > points.Length * points.Length)
                { //если по какой-либо причине (например, многоугольник задан по часовой стрелке) триангуляцию провести невозможно, выходим
                    triangles = null;
                    break;
                }

                count++;
            }

            if (triangles != null) //если триангуляция была проведена успешно
                triangles[trainPos] = new Triangle(points[ai], points[bi], points[ci]);
        }

        /// <summary>
        /// найти следущую нерассмотренную вершину
        /// </summary>
        /// <param name="points"> вершины</param>
        /// <param name="startPos"> позиция</param>
        /// <returns></returns>
        private int findNextNotTaken(PointF[] points, int startPos, bool[] taken)
        {
            startPos %= points.Length;
            if (!taken[startPos])
                return startPos;

            int i = (startPos + 1) % points.Length;
            while (i != startPos)
            {
                if (!taken[i])
                    return i;
                i = (i + 1) % points.Length;
            }

            return -1;
        }

        /// <summary>
        /// //левая ли тройка векторов
        /// </summary>
        /// <param name="a"> вершина А</param>
        /// <param name="b"> вершины В</param>
        /// <param name="c"> вершина С</param>
        /// <returns></returns>
        private bool isLeft(PointF a, PointF b, PointF c)
        {
            float abX = b.X - a.X;
            float abY = b.Y - a.Y;
            float acX = c.X - a.X;
            float acY = c.Y - a.Y;

            return abX * acY - acX * abY < 0;
        }

        /// <summary>
        /// находится ли точка p внутри треугольника abc
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        private bool isPointInside(PointF a, PointF b, PointF c, PointF p)
        {
            float ab = (a.X - p.X) * (b.Y - a.Y) - (b.X - a.X) * (a.Y - p.Y);
            float bc = (b.X - p.X) * (c.Y - b.Y) - (c.X - b.X) * (b.Y - p.Y);
            float ca = (c.X - p.X) * (a.Y - c.Y) - (a.X - c.X) * (c.Y - p.Y);

            return (ab >= 0 && bc >= 0 && ca >= 0) || (ab <= 0 && bc <= 0 && ca <= 0);
        }

        /// <summary>
        /// Может быть построен триугольник
        /// </summary>
        /// <param name="points"></param>
        /// <param name="ai"></param>
        /// <param name="bi"></param>
        /// <param name="ci"></param>
        /// <returns></returns>
        private bool canBuildTriangle(PointF[] points, int ai, int bi, int ci) //false - если внутри есть вершина
        {
            for (int i = 0; i < points.Length; i++) //рассмотрим все вершины многоугольника
                if (i != ai && i != bi && i != ci) //кроме троих вершин текущего треугольника
                    if (isPointInside(points[ai], points[bi], points[ci], points[i]))
                        return false;
            return true;
        }
    }
}
